#include <iostream>
#include <time.h>
using namespace std;

int main()
{
    const int size = 5;
    int array[size][size];

    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            array[i][j] = i + j; /*массив заполнится автоматически суммой индексов*/
            cout << array[i][j];
        }
        cout << "\n";
    }

    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    int cday = buf.tm_mday;
    int StringIndex = cday % size;
    int sum = 0;

    for (int j = 0; j < size; j++)
    {
    sum += array[StringIndex][j];
    }

    cout << "Current Day:" << cday << "\n";
    cout << "String Index:" << StringIndex << "\n";
    cout << "Sum of string:" << sum;
}
